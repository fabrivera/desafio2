# Repositorio de películas

> Es un proyecto basado en la prueba de Folcademy con la implementación
> de una **API REST** en node.js, Express y MongoDB corriendo en Heroku

# Link de la Web
[Ver Página web](https://movies-folcademy.herokuapp.com/)

# API REST

![API](https://bitbucket.org/fabrivera/desafio2/raw/075e1687aba4ec35005a3b03d8230ba1f50bc59f/src/assets/api.jpg)

[GitHub Api](https://github.com/fabrivera/api-movies)

> La Api se encuentra trabajando con algunas funciones de listar
> peliculas con paginación, subir películas, crear usuarios, entre
> otras... pero aún falta definir algunas funciones.

**Agregar películas a la API**

Debemos hace un POST a la url: https://movies-folcademy.herokuapp.com/api/movies

> Debemos enviar un multipath con image, name, description, category (movie, serie),
> sentence, rate (tipo number)

**Listar Json de películas**

La siguente url por metodo GET nos devuelve un Json: https://movies-folcademy.herokuapp.com/api/movies?limit=8&page=0

> Tenemos que enviar 2 parametros por medio de Query en la URL,
> cantidad a mostrar (limit), y paginación (page)

**Listar películas en web**

Como aún no tiene agregado un botón de paginación debemos hacerlo manual

> Igual que en el Json debemos incluir en la URL de la web page y limit,
> ej: https://movies-folcademy.herokuapp.com?limit=5&page=0.
>
> Si se agregan nuevas películas la paginación debe cambiar para que
> se vea reflejado. 
> Ej: limit=8&page=1 (mostrará la 2da pagina)
> EJ: limit=10&page=0 (mostrará 10 películas por página)

[Link del ejemplo](https://movies-folcademy.herokuapp.com?limit=5&page=0)