import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BackgroundComponent } from './background/background.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';



@NgModule({
  declarations: [
    HeaderComponent,
    BackgroundComponent,
    FooterComponent,
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    HeaderComponent,
    BackgroundComponent,
    FooterComponent
  ],
  providers: [AuthService]
})
export class LayoutModule { }
