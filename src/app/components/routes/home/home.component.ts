import { ViewportScroller } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { MoviesService } from 'src/app/services/movies.service';
import { IMovies } from 'src/interfaces/Movies';
import { IGetMovies } from 'src/interfaces/Services';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [MoviesService, AuthService]
})
export class HomeComponent implements OnInit {

  public userId: string = ''

  public filter:string = "all"
  public filterShow:string = "Todos"
  public totalList:number = 0
  public routeUrl: string = ''
  public movies_series: IMovies = []
  public showList: IMovies = []
  public info: any = {
    page: 1,
    totalPage: 1,
    pagination: []
  }

  constructor(
    private _moviesService: MoviesService,
    private User: AuthService,
    private route: ActivatedRoute,
    private viewportScroller: ViewportScroller
  ) { }

  async ngOnInit() {
    this.route.queryParams
      .subscribe(params => {
        if (params["page"]) this.info.page = Number(params["page"])
      }
    )
    this.getList(this.filter)

    const user = await this.User.get()
    if (user) this.userId = user.user.uid
  }

  public count:number = this.movies_series.length

  public getList(param:string) {
    let params: IGetMovies = {
      path: param,
      page: this.info.page.toString(),
      language: 'es'
    }

    this._moviesService.getTrending(params).subscribe({
      next: data => {
        this.movies_series = data.results
        this.totalList = data.total_results
        this.info.totalPage = data.total_pages
        this.info.pagination = []
        this.paginate(data)
      },
      error: err => {
        console.log(err)
      },
      complete: () => {
        this.showList = this.movies_series
        this.filter = param
        this.filterTranslate()
      }
    })
  }
 
  public searchNow(param:string) {
    const newList = this.movies_series.filter(movie => {
      return movie.title?.toLowerCase().includes(param) || movie.name?.toLowerCase().includes(param)
    })
    this.showList = newList

    // let page = this.info.page.toString()
    // if (pageReset) page = 1
    // this._moviesService.search(query,this.filter,page).subscribe({
    //   next: data => {
    //     this.movies_series = data.results
    //     this.totalList = data.total_results
    //     this.info.totalPage = data.total_pages
    //     this.info.pagination = []
    //     this.paginate(data)
    //   },
    //   error: err => {
    //     console.log(err)
    //   }
    // })
  }

  private paginate(data:any) {
    for(let i=-2; i <= 2; i++) {
      if(data.total_pages >= 3) {
        if (this.info.page < 3 ) {
          this.info.pagination.push(1 + i + 2)
        }
        else if (this.info.page + 2 >= data.total_pages) {
          this.info.pagination.push(data.total_pages + i - 2)
        }
        else {
          this.info.pagination.push(this.info.page + i)
        }
      }
      else if (data.total_pages === 2) {
        this.info.pagination = [1,2]
      }
      else {
        this.info.pagination = [1]
      }
    }
  }

  public toPageArrow(operator:string) {
    if (operator === '+') {
      if (this.info.page + 10 > this.info.totalPage) {
        this.info.page = this.info.totalPage
      }
      else {
        this.info.page = this.info.page + 10
      }
    }
    else if (operator === '-') {
      if (this.info.page - 10 > 0) {
        this.info.page = this.info.page - 10
      }
      else {
        this.info.page = 1
      }
    }
    
    this.getList(this.filter)
    this.viewportScroller.scrollToAnchor('list')
  }

  public toPage(page:string|number) {
    this.info.page = page
    this.getList(this.filter)
    this.viewportScroller.scrollToAnchor('list')
  }

  public filterTranslate() {
    if (this.filter === 'movie'){
      this.filterShow = 'Películas'
    }
    else if (this.filter === 'tv'){
      this.filterShow = 'Series'
    }
    else {
      this.filterShow = 'Todos'
    }
  }

}
