import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [AuthService]
})

export class DashboardComponent implements OnInit {

  peliculas:number = 0
  series:number = 0
  userId:string

  constructor(
    private User: AuthService,
    private _moviesService: MoviesService,
    private router: Router
  ) { }

  async ngOnInit() {
    const user = await this.User.get()
    if (user) {
      this.userId = user.user.uid
      this._moviesService.getItem(this.userId).subscribe(res => {
        res.forEach((element:any) => {
          if (element.payload.doc.data().media_type === 'movie') {
            this.peliculas ++
          }
          if (element.payload.doc.data().media_type === 'tv') {
            this.series ++
          }
        })
      })
    }
  }

}
