import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MoviesService } from 'src/app/services/movies.service';
import { IFullMovie } from 'src/interfaces/FullMovie';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {
  public type:string = ''
  public typeToShow:string = ''
  public id:string = ''
  public geners = []

  constructor(
    private route: ActivatedRoute,
    private _moviesService: MoviesService
  ) { }

  public movie: IFullMovie

  ngOnInit(): void {
    this.route.url
      .subscribe(params => {
        this.type = params[0].path
        if (params[0].path === 'tv') this.typeToShow = 'TV Show'
        else this.typeToShow = 'Movie'
        this.id = params[1].path
      })
    
    this._moviesService.getMovieById(this.type, this.id).subscribe({
      next: data => {
        this.movie = data
      },
      error: err => {
        console.log(err)
      },
      complete: () => {

      }
    })    
  }
  getGeners(geners:any) {
    const result:any = []
    geners.map((res:any) => {
      result.push(res.name)
    })
    return result.join(', ')
  }
}