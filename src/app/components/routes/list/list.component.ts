import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { MoviesService } from 'src/app/services/movies.service';
import { IMovies } from 'src/interfaces/Movies';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  userId:string
  movies_series: IMovies = []

  constructor(
    private User: AuthService,
    private _moviesService: MoviesService
  ) { }

  async ngOnInit() {
    const user = await this.User.get()
    if (user) {
      this.userId = user.user.uid
      this._moviesService.getItem(this.userId).subscribe(res => {
        this.movies_series = []
        res.forEach((element:any) => {
          this.movies_series.push({...element.payload.doc.data()})
        })
      })
    }
  }

}
