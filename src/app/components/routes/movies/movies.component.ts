import { ViewportScroller } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MoviesService } from 'src/app/services/movies.service';
import { IMovies } from 'src/interfaces/Movies';
import { IGetMovies } from 'src/interfaces/Services';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css'],
  providers: [MoviesService]
})
export class MoviesComponent implements OnInit {

  public routeUrl: string
  public totalList:number = 0
  public movies_series: IMovies = []
  public showList: IMovies = []
  public filter:string
  public info: any = {
    page: 1,
    totalPage: 1,
    pagination: []
  }

  constructor(
    private _moviesService: MoviesService,
    private route: ActivatedRoute,
    private viewportScroller: ViewportScroller
  ) { }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        if (params["page"]) this.info.page = params["page"]
      }
    )
    this.route.url
      .subscribe(params => {
        if (params[0].path === 'movies') {
          this.routeUrl = 'Películas'
          this.filter = 'movie'
        }
        else {
          this.routeUrl = "Series"
          this.filter = 'tv'
        }
      })

      this.getList()
  }

  public getList() {
    let params: IGetMovies = {
      path: 'movie',
      page: this.info.page.toString(),
      language: 'es'
    }
    if (this.routeUrl === 'Series') params.path = 'serie'

    this._moviesService.getMovies(params).subscribe({
      next: data => {
        this.movies_series = data.results
        this.totalList = data.total_results
        this.info.totalPage = data.total_pages
        this.info.pagination = []
        this.paginate(data)
      },
      error: err => {
        console.log(err)
      },
      complete: () => {
        this.showList = this.movies_series
      }
    })
  }

  public searchNow(param:string) {
    const newList = this.movies_series.filter(movie => {
      return movie.title?.toLowerCase().includes(param) || movie.name?.toLowerCase().includes(param)
    })
    this.showList = newList

    // let page = this.info.page.toString()
    // if (pageReset) page = 1
    // this._moviesService.search(query,this.filter,page).subscribe({
    //   next: data => {
    //     this.movies_series = data.results
    //     this.totalList = data.total_results
    //     this.info.totalPage = data.total_pages
    //     this.info.pagination = []
    //     this.paginate(data)
    //   },
    //   error: err => {
    //     console.log(err)
    //   }
    // })
  }

  private paginate(data:any) {
    for(let i=-2; i <= 2; i++) {
      if(data.total_pages >= 3) {
        if (this.info.page < 3 ) {
          this.info.pagination.push(1 + i + 2)
        }
        else if (this.info.page + 2 >= data.total_pages) {
          this.info.pagination.push(data.total_pages + i - 2)
        }
        else {
          this.info.pagination.push(this.info.page + i)
        }
      }
      else if (data.total_pages === 2) {
        this.info.pagination = [1,2]
      }
      else {
        this.info.pagination = [1]
      }
    }
  }

  public toPageArrow(operator:string) {
    if (operator === '+') {
      if (this.info.page + 10 > this.info.totalPage) {
        this.info.page = this.info.totalPage
      }
      else {
        this.info.page = this.info.page + 10
      }
    }
    else if (operator === '-') {
      if (this.info.page - 10 > 0) {
        this.info.page = this.info.page - 10
      }
      else {
        this.info.page = 1
      }
    }
    
    this.getList()
    this.viewportScroller.scrollToAnchor('list')
  }

  public toPage(page:string|number) {
    this.info.page = page
    this.getList()
    this.viewportScroller.scrollToAnchor('list')
  }

}