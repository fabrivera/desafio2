import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Páginas de Routes
import { HomeComponent } from './components/routes/home/home.component'
import { LoginComponent } from './components/routes/login/login.component'
import { Error404Component } from './components/routes/error404/error404.component'
import { MoviesComponent } from './components/routes/movies/movies.component';
import { ViewComponent } from './components/routes/view/view.component';
import { DashboardComponent } from './components/routes/dashboard/dashboard.component';
import { ListComponent } from './components/routes/list/list.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'login', component: LoginComponent },
  { path: 'series', component: MoviesComponent },
  { path: 'movies', component: MoviesComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'list', component: ListComponent },
  { path: 'movie/:id', component: ViewComponent},
  { path: 'tv/:id', component: ViewComponent},
  { path: '**', component: Error404Component }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }


