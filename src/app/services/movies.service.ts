import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IGetMovies } from '../../interfaces/Services'
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { IMovie, MediaType } from 'src/interfaces/Movies';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  private token:string = 'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIzZDViMjNlNWU0MDkyNzU3OWFmM2Q5ZTQ5MmZkYTA3YiIsInN1YiI6IjYyMTUxMGI4MGU0ZmM4MDAxZGNlZmMzZCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.wD6A6V0sQI1Bmtk9xikxXzQ1IBWK_CkPyUQCn1oZf2E'
  private apiUrl:string = 'https://api.themoviedb.org/3'

  constructor(
    private _http: HttpClient,
    private firestore: AngularFirestore
    ) { }

  
  getTrending(param:any): Observable<any> {
    let urlPath:string = 'all'
    if (param.path === 'movie') urlPath = 'movie' 
    if (param.path === 'tv') urlPath = 'tv'
    
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json;charset=utf-8')
      .set('Authorization', 'Bearer ' + this.token)
    const params = new HttpParams()
      .set('page', param.page)
      .set('language', param.language)
    return this._http.get(`${this.apiUrl}/trending/${urlPath}/week`, {headers, params})
  }

  getMovies(param:IGetMovies): Observable<any> {
    let urlPath:string = 'movie'
    if (param.path === 'serie') urlPath = 'tv'

    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json;charset=utf-8')
      .set('Authorization', 'Bearer ' + this.token)
    const params = new HttpParams()
      .set('page', param.page)
      .set('language', param.language)
    return this._http.get(`${this.apiUrl}/${urlPath}/popular`, {headers, params})
  }

  getMovieById(type:string, id:string): Observable<any> {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json;charset=utf-8')
      .set('Authorization', 'Bearer ' + this.token)
    const params = new HttpParams()
      .set('language', 'es')
    return this._http.get(`${this.apiUrl}/${type}/${id}`, {headers, params})
  }

  /*search(query:string, type:string, page:string): Observable<any> {
    if (type === 'all') type = 'multi'
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json;charset=utf-8')
      .set('Authorization', 'Bearer ' + this.token)
    const params = new HttpParams()
      .set('language', 'es')
      .set('adult', 'false')
      .set('query', query)
      .set('page', page)
    return this._http.get(`${this.apiUrl}/search/${type}`, {headers, params})
  }*/

  addItem(userId:string, item:IMovie): Promise<any> {
    return this.firestore.collection('usuarios').doc(`${userId}`).collection('list').add(item)
      .then(res => {
        return res.id
      })
  }
  getItem(userId:string): Observable<any> {
    return this.firestore.collection('usuarios').doc(`${userId}`).collection('list').snapshotChanges()
  }
  deleteItem(userId:string, id:string): Promise<any> {
    return this.firestore.collection(`usuarios/${userId}/list`).doc(id).delete()
  }
}


//https://api.themoviedb.org/3/search/multi?&page=1&include_adult=false&query=jhon