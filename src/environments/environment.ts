// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBvs9jOHzd-fwHYwWjlMfgdh0CyzNTnkf8",
    authDomain: "database-2ca73.firebaseapp.com",
    projectId: "database-2ca73",
    storageBucket: "database-2ca73.appspot.com",
    messagingSenderId: "549239447305",
    appId: "1:549239447305:web:59bef2f52dd569b95dbdd8",
    measurementId: "G-Y16JBY9BMM"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
