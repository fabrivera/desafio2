export interface Result {
    page:          number;
    results:       IMovie[];
    total_pages:   number;
    total_results: number;
}

export interface IMovie {
    overview:          string;
    release_date?:     Date;
    adult?:            boolean;
    backdrop_path:     string;
    vote_count:        number;
    genre_ids:         number[];
    video?:            boolean;
    original_language: OriginalLanguage;
    original_title?:   string;
    poster_path:       string;
    id:                number;
    title?:            string;
    vote_average:      number;
    popularity:        number;
    media_type:        MediaType;
    original_name?:    string;
    origin_country?:   string[];
    name?:             string;
    first_air_date?:   Date;
}

export enum MediaType {
    Movie = "movie",
    Tv = "tv",
}

export enum OriginalLanguage {
    En = "en",
    Ja = "ja",
}

export interface IMovies extends Array<IMovie> {}