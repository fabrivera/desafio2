export interface IGetMovies {
    path: string,
    page: string,
    language: string
}